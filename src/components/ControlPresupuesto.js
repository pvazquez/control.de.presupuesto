import React, { Fragment } from 'react';
import { cambiarColorPresupuesto } from '../helpers';

const ControlPresupuesto = ({presupuesto, saldo}) => (
    <Fragment>
        <div className="alert alert-primary">Presupuesto: ${presupuesto}</div>
        <div className={cambiarColorPresupuesto(presupuesto, saldo)}>Restante: ${saldo}</div>
    </Fragment>
)

export default ControlPresupuesto;
