import React, {useState} from 'react'
import Error from './Error';

import shortid from 'shortid';

const FormularioGastos = ({guardarGasto, guardarCrearGasto}) => {

  const [nombre_gasto, guardarNombreGasto] = useState('');
  const [valor_gasto, guardarValorGasto] = useState(0);
  const [error, guardarError] = useState(false);

  const agregarGasto = (event) => {
      event.preventDefault();

      //validar
      if(valor_gasto <= 0 || isNaN(valor_gasto) || nombre_gasto.trim() === '') {
          guardarError(true);
          return;
      }
          guardarError(false);

      //construir el gasto
      const gasto = {
          nombre_gasto,
          valor_gasto,
          id: shortid.generate()
      }

      //pasar el gasto al componente principal
      guardarGasto(gasto);
      guardarCrearGasto(true);

      //resetear el form
      guardarNombreGasto('');
      guardarValorGasto(0);
  }

  return(
      <form onSubmit={agregarGasto}>
          <h2>Agrega tus gastos aquí</h2>
          {error ? <Error mensaje='Campos incompletos o invalidos'/> : null}
          <div className="campo">
              <label>Nombre de gasto</label>
              <input
                  className="u-full-width"
                  type="text"
                  placeholder="Ej: Transporte"
                  value={nombre_gasto}
                  onChange={event => guardarNombreGasto(event.target.value)}
              />
          </div>
          <div className="campo">
              <label>Gasto</label>
              <input
                  className="u-full-width"
                  type="number"
                  placeholder="300"
                  value={valor_gasto}
                  onChange={event => guardarValorGasto( parseInt(event.target.value, 10) )}
              />
          </div>
          <input
              className="button-primary u-full-width"
              type="submit"
              value="Agregar Gasto"
          />
      </form>
  )
}

export default FormularioGastos;
