import React, { Fragment, useState } from 'react';
import Error from './Error';

import PropTypes from 'prop-types';


const Pregunta = ({guardarPresupuesto, guardarSaldo, mostrarPregunta}) => {

  const [cantidad, guardarCantidad ] = useState(0);
  const [error, guardarError] = useState(false);

  //Atrapo el valor en el state
  const definirPresupuesto = event => {
      guardarCantidad( parseInt(event.target.value, 10))
  }

  const handleSubmit = event => {
      event.preventDefault();

      if(cantidad <= 0 || isNaN(cantidad)) {
          guardarError(true);
          return;
      } else {
          guardarError(false);
          guardarPresupuesto(cantidad);
          guardarSaldo(cantidad);
          mostrarPregunta(false);
      }
  }


  return (
      <Fragment>
          <h2>Coloca tu presupuesto</h2>
          {error ? <Error mensaje='No es un presupuesto valido'/> : null}
          <form onSubmit={handleSubmit}>
              <input
                  className="u-full-width"
                  type="number"
                  placeholder="Coloca tu presupuesto"
                  onChange={definirPresupuesto}
              />
              <input
                  className="button-primary u-full-width"
                  type="submit"
                  value="Definir presupuesto"
              />
          </form>
      </Fragment>
  )

}

//Ejemplo de como documentar las propiedades que recibe el componente
Pregunta.propTypes = {
    guardarPresupuesto: PropTypes.func.isRequired,
    guardarSaldo: PropTypes.func.isRequired,
    mostrarPregunta: PropTypes.func.isRequired
}

export default Pregunta;
