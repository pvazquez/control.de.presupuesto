import React from 'react'

const Error = ({mensaje}) => (

    <p className="alter alert-danger error">{ mensaje } </p>
)

export default Error;
