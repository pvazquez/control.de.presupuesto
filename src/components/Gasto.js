import React from 'react'

const Gasto = ({gasto}) => (
    <li className="gastos">
        <p>
            {gasto.nombre_gasto}
            <span className="gasto">$ {gasto.valor_gasto}</span>
        </p>
    </li>
)

export default Gasto;
