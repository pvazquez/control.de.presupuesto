import React, { useState, useEffect } from 'react';
import Pregunta from './components/Presupuesto';
import FormularioGastos from './components/FormularioGastos';
import Listado from './components/Listado';
import ControlPresupuesto from './components/ControlPresupuesto';

function App() {

  const [presupuesto, guardarPresupuesto] = useState(0);
  const [saldo, guardarSaldo] = useState(0);
  const [pregunta, mostrarPregunta]  = useState(true);
  const [gastos, guardarGastos] = useState([]);
  const [gasto, guardarGasto] = useState({});
  const [crear_gasto, guardarCrearGasto] = useState(false);

  //useEffect que actualiza el saldo
  useEffect( () => {
      if(crear_gasto) {
          //Agrega el nuevo presupuesto
          guardarGastos([
              ...gastos,
              gasto
          ])

          //Calcular saldo restando del presupuesto
          const presupuestoRestante = saldo - gasto.valor_gasto;
          guardarSaldo(presupuestoRestante);

          guardarCrearGasto(false);
      }

  }, [gasto, crear_gasto, gastos, saldo]);


  return (
    <div className="container">
        <header>
            <h1>Presupuesto</h1>
            <div className="contenido-principal contenido">
                { pregunta ?
                  //Los parentesis que siguen no son necesarios pero se
                  //recomiendo ponerlos para mostrar el return implicito.
                  //En FormularioGastos.js hago lo mismo sin parentesis()
                (
                  <Pregunta
                    guardarPresupuesto = {guardarPresupuesto}
                    guardarSaldo = {guardarSaldo}
                    mostrarPregunta = {mostrarPregunta}
                  />
                )
                :
                (
                <div className="row">
                    <div className="one-half column">
                        <FormularioGastos
                            guardarGasto={guardarGasto}
                            guardarCrearGasto={guardarCrearGasto}
                        />
                    </div>
                    <div className="one-half column">
                        <Listado
                            gastos={gastos}
                        />
                        <ControlPresupuesto
                            presupuesto={presupuesto}
                            saldo={saldo}
                        />
                    </div>
                </div>
                )
                }
            </div>
        </header>
    </div>
  );
}

export default App;
