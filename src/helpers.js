export const cambiarColorPresupuesto = (presupuesto, saldo) => {

    let clase;

    if(saldo <= presupuesto*0.25) {
        clase = 'alert alert-danger';
    } else if(saldo<= presupuesto*0.75) {
        clase = 'alert alert-warning';
    } else {
        clase = 'alert alert-success';
    }

    return clase;
}
